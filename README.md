
This repository was used to prepare an introductory course to the use of git and gitlab at the INRAE, specifically for research. 

The course was given on Feb 16th, 2024 in BIOGECO.

The slides for the course are available here: 

[https://umr-1202-biogeco.pages.mia.inra.fr/initiation_git](https://umr-1202-biogeco.pages.mia.inra.fr/initiation_git)

--- 

## Contributors:

François Ehrenmann

Ludovic Duvaux

Benjamin Brachi


![An image from the internet](https://vickysteeves.gitlab.io/repro-papers/img/final-doc.jpg)

